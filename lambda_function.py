import json

def lambda_handler(event, context):

    return {
        'statusCode': 200,
        'body': json.dumps({'input': event, 'output': 'Response from lambda'})
    }